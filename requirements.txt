-e git+https://github.com/brunoxbk/django-admin-multiupload.git@4420b82da0451ab500eee3bef2d4a4033c88e78c#egg=admin_multiupload
appdirs==1.4.3
Django==1.9
django-anymail==0.8
django-filemanager-lte==0.0.7
django-reversion==2.0.8
-e git+https://github.com/brunoxbk/django-suit.git@488b779cba7ed711901529624b3ec7381589745b#egg=django_suit
-e git+https://github.com/benoitc/gunicorn.git#egg=gunicorn
django-tinymce==2.6.0
django-versatileimagefield==1.6.3
django-widget-tweaks==1.4.1
olefile==0.44
packaging==16.8
Pillow==4.0.0
pyparsing==2.1.10
python-decouple==3.0
python-http-client==3.2.1
python-telegram-bot==12.1.1
requests==2.14.2
six==1.10.0
whitenoise==4.1.3
psycopg2-binary==2.8.3
django-ckeditor==5.2.2
django-materialize-form
django-sendgrid-v5==0.8.1
django-recaptcha==2.0.0
Django==1.9