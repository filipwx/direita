#!/bin/bash
cd /var/www/pativo.128bits.cc/principio_ativo
git pull origin master
docker exec pativo sh -c "pip3 install -r /var/www/requirements.txt"
docker exec pativo sh -c "/var/www/manage.py migrate --noinput"
docker exec pativo sh -c "/var/www/manage.py collectstatic --noinput"
docker exec pativo pkill python3.5
docker exec -d pativo gunicorn --pythonpath /var/www principio_ativo.wsgi -b 0.0.0.0:8010