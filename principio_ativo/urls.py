"""principio_ativo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^manage-cms/', admin.site.urls),
    url(r'', include('apps.website.urls')),
    url(r'acesso/', include('apps.usuario.urls')),
    url(r'contato/', include('apps.contato.urls')),
    url(r'grupos/', include('apps.grupo.urls')),
    url(r'eventos/', include('apps.evento.urls')),
    url(r'artigos/', include('apps.artigos.urls')),
    url(r'^manage-cms/filemanager/', include('filemanager.urls', namespace='filemanager')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^reset-password/$', auth_views.password_reset, {'template_name': 'usuario/password_reset_form.html', 'post_reset_redirect': '/reset-password/done/'}, name='password_reset'),
    url(r'^reset-password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, { 'template_name': 'usuario/password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^reset-password/done/$', auth_views.password_reset_done, { 'template_name': 'usuario/password_reset_done.html' }, name='password_reset_done'),
    url(r'^reset-password/complete/$', auth_views.password_reset_complete, { 'template_name': 'usuario/password_reset_complete.html' }, name='password_reset_complete'),
    # url(r'^accounts/', include('django.contrib.auth.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
