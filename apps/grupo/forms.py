from django import forms
from .models import *
from django.forms import ModelForm

class GruposcreateForm(ModelForm):
    class Meta:
        model = Grupos
        exclude = ('codigo', 'creation_user', 'slug')
