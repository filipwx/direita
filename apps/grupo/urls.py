from django.conf.urls import url
from django.contrib.auth.views import logout, login
from django.conf import settings

from . import views

app_name = 'grupo'

urlpatterns = [
    url(r'^lista-de-grupos/$', views.GruposView.as_view(), name='grupos'),
    url(r'^criar-grupo/$', views.GruposCreateView.as_view(), name='criar-grupos'),
    url(r'^grupo/(?P<slug>[-\w]+)/$', views.GruposInternoView.as_view(), name='grupo'),
    url(r'^editar-grupo/(?P<slug>[-\w]+)/$', views.GruposEditView.as_view(), name='editar-grupo'),
    url(r'^delete-grupo/(?P<slug>[-\w]+)/$', views.GrupoDeleteView.as_view(), name='apagar-grupo'),
]
