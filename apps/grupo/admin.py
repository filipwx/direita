from django.contrib import admin
from apps.core.admin import DefaultAdmin
from .models import Grupos

@admin.register(Grupos)
class GruposAdmin(DefaultAdmin):
    list_display = ('nome', 'subcategoria', 'telefone', 'email', 'creation_user', 
                    'criado_em', 'plataforma1')
