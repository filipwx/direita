from django.db import models
import random
import datetime
from apps.usuario.models import Usuario
from django.utils.text import slugify
from random import randrange

class Grupos(models.Model):

    nome = models.CharField(max_length=355, null=True, blank=True)
    descricao = models.CharField(max_length=355, null=True, blank=True)
    categoria = models.CharField(max_length=355, null=True, blank=True)
    subcategoria = models.CharField(max_length=355, null=True, blank=True)
    lat = models.CharField(max_length=355, null=True, blank=True)
    lon = models.CharField(max_length=355, null=True, blank=True)
    estado = models.CharField(max_length=355, null=True, blank=True, default='nao_informado')
    endereco = models.CharField(max_length=355, null=True, blank=True)
    telefone = models.CharField(max_length=355, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    codigo = models.CharField(max_length=355, null=True, blank=True)
    creation_user = models.ForeignKey(Usuario, on_delete=models.PROTECT, related_name='creation', verbose_name='Usuário Inclusão')
    slug = models.SlugField(max_length=700)

    plataforma1 = models.CharField(max_length=355, null=True, blank=True)
    link1 = models.CharField(max_length=355, null=True, blank=True)
    plataforma2 = models.CharField(max_length=355, null=True, blank=True)
    link2 = models.CharField(max_length=355, null=True, blank=True)
    plataforma3 = models.CharField(max_length=355, null=True, blank=True)
    link3 = models.CharField(max_length=355, null=True, blank=True)
    plataforma4 = models.CharField(max_length=355, null=True, blank=True)
    link4 = models.CharField(max_length=355, null=True, blank=True)
    plataforma5 = models.CharField(max_length=355, null=True, blank=True)
    link5 = models.CharField(max_length=355, null=True, blank=True)
    criado_em = models.DateTimeField(
        verbose_name='Criado em', auto_now_add=True)
    modificado_em = models.DateTimeField(
        verbose_name='Modificado em', auto_now=True)

    class Meta:
        verbose_name = "Grupo"
        verbose_name_plural = "Grupos"

    def save(self, *args, **kwargs):
        self.codigo = random.getrandbits(128)
        self.slug = slugify(self.nome)
        while True:
            noticia = Grupos.objects.filter(slug=self.slug)
            if noticia.exists():
                self.slug = '{}{}'.format(self.slug, randrange(10000000000))
            else:
                break
        super(Grupos, self).save(*args, **kwargs)
