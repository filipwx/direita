from django.views import generic
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView, FormView, DetailView, DeleteView
from django.views.generic.edit import UpdateView
from django.conf.urls import url
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from .models import *
from .forms import GruposcreateForm
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
# from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
import telegram

class GruposView(ListView):
    template_name = 'grupos.html'
    model = Grupos
    paginate_by = 5
    context_object_name = 'grupos_list'

    def get_context_data(self, **kwargs):
        qs = Grupos.objects.all().order_by('-criado_em')

        keyword1 = self.request.GET.get('estado')
        keyword2 = self.request.GET.get('nome')
        if keyword1:
            qs = Grupos.objects.filter(estado__icontains=keyword1)
            context = super(GruposView, self).get_context_data(**kwargs)
            if str(self.request.user) == "AnonymousUser":
                user_loged = False
                pass
            else:
                user_loged = True
                pass
            context.update(dict(
                grupos_list=qs,
                user_loged=user_loged
            ))
            return context
        if keyword2:
            qs = Grupos.objects.filter(nome__icontains=keyword2)
            context = super(GruposView, self).get_context_data(**kwargs)
            if str(self.request.user) == "AnonymousUser":
                user_loged = False
                pass
            else:
                user_loged = True
                pass
            context.update(dict(
                grupos_list=qs,
                user_loged=user_loged
            ))
            return context

        context = super(GruposView, self).get_context_data(**kwargs)
        if str(self.request.user) == "AnonymousUser":
            user_loged = False
            pass
        else:
            user_loged = True
            pass
        context.update(dict(
            grupos_list=qs,
            user_loged=user_loged
        ))
        return context

class GruposCreateView(LoginRequiredMixin, generic.CreateView):
    login_url = '/acesso/login/?needlogin=true'
    model = Grupos
    form_class = GruposcreateForm
    template_name = 'grupo_create.html'

    def form_valid(self, form):
        obj = form.save(commit=False)
        if obj.lat is None or obj.lat is '':
            return render(self.request, 'grupo_create.html', {'form': form})
        obj.creation_user = self.request.user
        obj.save()
        messages.success(self.request, 'Conta criada com sucesso')
        email_content = 'Novo cadastro de GRUPO '
        email_content += '\nNome: {}'.format(form.cleaned_data['nome'])
        email_content += '\nDescrição: {}'.format(
            form.cleaned_data['descricao'])
        email_content += '\nEstado: {}'.format(form.cleaned_data['estado'])
        bot = telegram.Bot('752881370:AAHKmpMXALAUzt1d89UUvoCkayq5GPXqO0Y')
        chat_id = -1001440430219
        try:
            bot.send_message(chat_id=chat_id, text=email_content)
            pass
        except Exception as e:
            print(e)
            pass
        return redirect('website:home')
    def form_invalid(self, form):
        print(form)
        return render(self.request, 'grupo_create.html', {'form': form})


class GruposInternoView(LoginRequiredMixin, DetailView):
    login_url = '/acesso/login/'
    template_name = 'grupo.html'

    model = Grupos

    def get_context_data(self, **kwargs):
        context = super(GruposInternoView, self).get_context_data(**kwargs)

        context.update(dict(
            noticias_list=Grupos.objects.all().order_by("nome"),
        ))

        return context


class GruposEditView(UpdateView):
    template_name = 'grupo_edit.html'
    form_class = GruposcreateForm
    model = Grupos

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()
        messages.success(self.request, 'Grupo editado com sucesso')
        return redirect('website:home')


class GrupoDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/acesso/login/?needlogin=true'
    template_name = 'grupo_confirm_delete.html'
    success_url = '/grupos/lista-de-grupos/'
    model = Grupos

    def get_object(self, queryset=None):
        obj = super(GrupoDeleteView, self).get_object()
        if not obj.creation_user == self.request.user:
            raise Http404
        return obj
