from django.db import models

# Create your models here.
class Receitas(models.Model):

    SIM_NAO = (
        (u'sim', u'Sim'),
        (u'nao', u'Não'),
    )

    UM_TRES = (
        (u'nao', u'Não'),
        (u'um_pouco', u'Um pouco'),
        (u'muito', u'Muito'),
    )

    FUNCIONAMENTO_INTESTINAL = (
        ('normal', 'Normal'),
        ('lento', 'Lento'),
        ('muito_lento', 'Muito Lento'),
        ('acelerado', 'Acelerado')
    )

    COME_MAIS = (
        ('massas', 'Massas (arroz, macarrão, pão, bolo)'),
        ('gordura', 'Comidas gordurosas (ex: churrasco)'),
        ('doces', 'Doces e Guloseimas'),
    )

    HIPERTROFIA = (
        ('hipertrofia', 'Hipertrofia'),
        ('emagrecimento', 'Emagrecimento'),
        ('reducao', 'Redução de Gordura Localizada'),
        ('melhorar', 'Melhorar performance no treino'),
        ('outros', 'Outros')
    )

    nome = models.CharField(max_length=355)
    email = models.EmailField()
    telefone = models.CharField(max_length=355)
    cidade = models.CharField(max_length=355)
    observacoes = models.TextField()
    receita1 = models.FileField('Receita 1', upload_to='receitas/%Y/%m/', blank=True, null=True)
    receita2 = models.FileField('Receita 2', upload_to='receitas/%Y/%m/', blank=True, null=True)
    receita3 = models.FileField('Receita 3', upload_to='receitas/%Y/%m/', blank=True, null=True)
    formula_fitness = models.BooleanField(default=False)
    retencao = models.CharField(
        'Retenção de Liquidos (Inchaços)',
        max_length=255,
        choices=UM_TRES,
        null=True, blank=True
    )
    funcionamento = models.CharField(
        'Funcionamento Intestinal',
        max_length=255,
        choices=FUNCIONAMENTO_INTESTINAL,
        null=True, blank=True
    )
    ansiedade = models.CharField(
        'Ansiedade',
        max_length=255,
        choices=UM_TRES,
        null=True, blank=True
    )
    durante_refeicoes = models.CharField(
        'Come muito durante as principais refeições',
        max_length=255,
        choices=SIM_NAO,
        null=True, blank=True
    )
    entre_refeicoes = models.CharField(
        'Come muito entre as refeições (hábito de beliscar)',
        max_length=255,
        choices=SIM_NAO,
        null=True, blank=True
    )
    come_mais = models.CharField(
        'Come mais o que',
        choices=COME_MAIS,
        max_length=255,
        null=True, blank=True
    )
    alergia = models.CharField(
        'Tem alergia a algum medicamento',
        choices=SIM_NAO,
        max_length=255,
        null=True, blank=True
    )
    alergia_texto = models.CharField(
        'Alergia aos medicamentos',
        max_length=755,
        null=True, blank=True
    )
    medicamento = models.CharField(
        'Faz uso de algum medicamento',
        choices=SIM_NAO,
        max_length=255,
        null=True, blank=True
    )
    doenca = models.CharField(
        'Tem alguma doença/problema crônico',
        choices=SIM_NAO,
        max_length=255,
        null=True, blank=True
    )
    atividade = models.CharField(
        'Faz alguma ativdade fisica',
        choices=SIM_NAO,
        max_length=255,
        null=True, blank=True
    )
    fuma = models.CharField(
        'Fuma',
        choices=SIM_NAO,
        max_length=255,
        null=True, blank=True
    )
    alcool = models.CharField(
        'Consome bebida alcoólica',
        choices=SIM_NAO,
        max_length=255,
        null=True, blank=True
    )
    procedimento_estetico = models.CharField(
        'Faz algum procedimento estético',
        choices=SIM_NAO,
        max_length=255,
        null=True, blank=True
    )
    objetivo = models.CharField(
        'Qual o seu objetivo',
        choices=HIPERTROFIA,
        max_length=255,
        null=True, blank=True
    )
    descricao_objetivo = models.CharField(
        'Descrição do objetivo',
        max_length=1555,
        null=True, blank=True
    )


    class Meta:
        verbose_name = "Receitas"
        verbose_name_plural = "Receitas"
