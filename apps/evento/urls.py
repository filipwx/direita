from django.conf.urls import url
from django.contrib.auth.views import logout, login
from django.conf import settings

from . import views

app_name = 'evento'

urlpatterns = [
    url(r'^lista-de-eventos/$', views.EventoView.as_view(), name='eventos'),
    url(r'^evento/(?P<slug>[-\w]+)/$', views.EventoInternoView.as_view(), name='evento'),
    url(r'^criar-evento/$', views.EventoCreateView.as_view(), name='criar-evento'),
    url(r'^editar-evento/(?P<slug>[-\w]+)/$', views.EventoEditView.as_view(), name='editar-evento'),
    url(r'^delete-evento/(?P<slug>[-\w]+)/$', views.EventoDeleteView.as_view(), name='apagar-evento'),
]
