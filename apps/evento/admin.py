from django.contrib import admin
from apps.core.admin import DefaultAdmin
from .models import Evento


@admin.register(Evento)
class EventoAdmin(DefaultAdmin):
  list_display = ('titulo', 'inicio', 'preco', 'endereco', 'criado_em', 'creation_user')

# Register your models here.
