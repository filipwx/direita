from django.views import generic
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView, FormView, DetailView, DeleteView
from django.views.generic.edit import UpdateView
from django.conf.urls import url
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from .models import *
from .forms import EventoscreateForm
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
import telegram

# Create your views here.

class EventoView(ListView):
  template_name = 'eventos.html'
  model = Evento
  paginate_by = 5
  context_object_name = 'eventos_list'

  def get_context_data(self, **kwargs):
        qs = Evento.objects.all().order_by('-criado_em')

        keyword1 = self.request.GET.get('estado')
        keyword2 = self.request.GET.get('titulo')
        if keyword1:
            qs = Evento.objects.filter(estado__icontains=keyword1)
            context = super(EventoView, self).get_context_data(**kwargs)
            if str(self.request.user) == "AnonymousUser":
                user_loged = False
                pass
            else:
                user_loged = True
                pass
            context.update(dict(
                eventos_list=qs,
                user_loged=user_loged
            ))
            return context
        if keyword2:
            qs = Evento.objects.filter(titulo__icontains=keyword2)
            context = super(EventoView, self).get_context_data(**kwargs)
            if str(self.request.user) == "AnonymousUser":
                user_loged = False
                pass
            else:
                user_loged = True
                pass
            context.update(dict(
                eventos_list=qs,
                user_loged=user_loged
            ))
            return context

        context = super(EventoView, self).get_context_data(**kwargs)
        if str(self.request.user) == "AnonymousUser":
            user_loged = False
            pass
        else:
            user_loged = True
            pass
        context.update(dict(
            eventos_list=qs,
            user_loged=user_loged
        ))
        return context


class EventoInternoView(LoginRequiredMixin, DetailView):
    login_url = '/acesso/login/?needlogin=true'
    template_name = 'evento.html'

    model = Evento

    def get_context_data(self, **kwargs):
        context = super(EventoInternoView, self).get_context_data(**kwargs)

        context.update(dict(
            noticias_list=Evento.objects.all().order_by("-criado_em"),
        ))

        return context


class EventoCreateView(LoginRequiredMixin, generic.CreateView):
    login_url = '/acesso/login/?needlogin=true'
    model = Evento
    form_class = EventoscreateForm
    template_name = 'evento_create.html'

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.creation_user = self.request.user
        obj.save()
        messages.success(self.request, 'Evento criado com sucesso')
        email_content = 'Novo cadastro de EVENTO '
        email_content += '\nTitulo: {}'.format(form.cleaned_data['titulo'])
        email_content += '\nInicio: {}'.format(form.cleaned_data['inicio'])
        email_content += '\nEstado: {}'.format(form.cleaned_data['estado'])
        bot = telegram.Bot('752881370:AAHKmpMXALAUzt1d89UUvoCkayq5GPXqO0Y')
        chat_id = -1001440430219
        try:
            bot.send_message(chat_id=chat_id, text=email_content)
            pass
        except Exception as e:
            print(e)
            pass
        return redirect('evento:eventos')

    def form_invalid(self, form):
        print(form)
        return render(self.request, 'evento_create.html', {'form': form})


class EventoEditView(LoginRequiredMixin, UpdateView):
    login_url = '/acesso/login/?needlogin=true'
    template_name = 'evento_edit.html'
    form_class = EventoscreateForm
    model = Evento

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()
        messages.success(self.request, 'Evento editado com sucesso')
        return redirect('evento:eventos')

class EventoDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/acesso/login/?needlogin=true'
    template_name = 'evento_confirm_delete.html'
    success_url = '/eventos/lista-de-eventos/'
    model = Evento

    def get_object(self, queryset=None):
        obj = super(EventoDeleteView, self).get_object()
        if not obj.creation_user == self.request.user:
            raise Http404
        return obj
