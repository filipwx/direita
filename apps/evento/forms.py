from django import forms
from .models import *
from django.forms import ModelForm


class EventoscreateForm(ModelForm):
    class Meta:
        model = Evento
        exclude = ('creation_user', 'slug')
