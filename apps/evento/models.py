from django.db import models
from django.utils.text import slugify
import random
from random import randrange

# Create your models here.
class Evento(models.Model):

  titulo = models.CharField(max_length=355, null=True, blank=True)
  descricao = models.TextField(null=True, blank=True)
  inicio = models.DateField(null=True, blank=True, auto_now=False, auto_now_add=False)
  preco = models.DecimalField(max_digits=5, decimal_places=2)
  link = models.URLField(null=True, blank=True)
  lat = models.CharField(max_length=355, null=True, blank=True)
  lon = models.CharField(max_length=355, null=True, blank=True)
  endereco = models.CharField(max_length=355, null=True, blank=True)
  estado = models.CharField(max_length=355, null=True, blank=True, default='nao_informado')
  slug = models.SlugField(max_length=700)
  creation_user = models.ForeignKey('usuario.Usuario', verbose_name='Usuário Inclusão', on_delete=models.CASCADE)
  criado_em = models.DateTimeField(
      verbose_name='Criado em', auto_now_add=True)
  modificado_em = models.DateTimeField(
      verbose_name='Modificado em', auto_now=True)

  class Meta:
    verbose_name = "Evento"
    verbose_name_plural = "Eventos"

  def save(self, *args, **kwargs):
    self.codigo = random.getrandbits(128)
    self.slug = slugify(self.titulo)
    while True:
        noticia = Evento.objects.filter(slug=self.slug)
        if noticia.exists():
            self.slug = '{}{}'.format(self.slug, randrange(10000000000))
        else:
            break
    super(Evento, self).save(*args, **kwargs)
