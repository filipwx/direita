from django import forms
from apps.contato.models import Contato
import requests


class ContactForm(forms.ModelForm):
  class Meta:
    model = Contato
    fields = ('nome', 'email', 'texto')
