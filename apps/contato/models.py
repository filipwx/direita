from django.db import models

# Create your models here.
class Contato(models.Model):
  nome = models.CharField('Nome', max_length=255, null=False, blank=False, default='')
  email = models.EmailField('E-mail', max_length=70, unique=False)
  texto = models.TextField('Texto', max_length=600, null=False, blank=False)
  status = models.BooleanField("Lida", default=False)
  criado_em = models.DateTimeField(blank=True, auto_now_add=True)
  alterado_em = models.DateTimeField(blank=True, auto_now=True)

  class Meta:
    verbose_name = 'Contato'
    verbose_name_plural = 'Contatos'

  def __str__(self):
    return '{}'.format(self.nome)
