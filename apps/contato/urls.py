from django.conf.urls import url
from django.contrib.auth.views import logout, login
from django.conf import settings

from . import views

app_name = 'contato'

urlpatterns = [
    url(r'^contato/$', views.ContatoCreateView.as_view(), name='contato'),
]
