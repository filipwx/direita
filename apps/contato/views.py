import json
import urllib
from django.views import generic
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView, FormView, DetailView, DeleteView
from django.views.generic.edit import UpdateView
from django.conf.urls import url
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from .models import *
from .forms import ContactForm
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
import telegram


# Create your views here.


class ContatoCreateView(generic.CreateView):
    model = Contato
    form_class = ContactForm
    template_name = 'contato_create.html'

    def form_valid(self, form):
        recaptcha_response = self.request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.RECAPTCHA_PRIVATE_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req =  urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())

        if result['success']:
            obj = form.save(commit=False)
            obj.save()
            messages.success(self.request, 'Contato salvo com sucesso')
            email_content = 'Novo cadastro de CONTATO '
            email_content += '\nNome: {}'.format(form.cleaned_data['nome'])
            email_content += '\nEmail: {}'.format(form.cleaned_data['email'])
            email_content += '\nTexto: {}'.format(form.cleaned_data['texto'])
            bot = telegram.Bot('752881370:AAHKmpMXALAUzt1d89UUvoCkayq5GPXqO0Y')
            chat_id = -1001440430219
            try:
                bot.send_message(chat_id=chat_id, text=email_content)
                pass
            except Exception as e:
                print(e)
                pass

            return redirect('contato:contato')
        else:
            messages.error(self.request, 'reCAPTCHA invalida. Tente novamente.')
            return render(self.request, 'contato_create.html', {'form': form})

    def form_invalid(self, form):
        print(form)
        return render(self.request, 'contato_create.html', {'form': form})
