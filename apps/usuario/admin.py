from django.contrib import admin
from apps.core.admin import DefaultAdmin
from .actions import export_as_csv_action

from .models import Usuario
# Register your models here.


@admin.register(Usuario)
class UsuarioAdmin(DefaultAdmin):
    list_display = ('email', 'nome', 'username')
    actions = [export_as_csv_action("Exportar selecionados", fields=['email', 'nome', 'username'])]
