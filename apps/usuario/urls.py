from django.conf.urls import url
from django.contrib.auth.views import logout, login
from django.conf import settings

from . import views

app_name = 'usuario'

urlpatterns = [
    url(r'^cadastro/$', views.SignUp.as_view(), name='signup'),
    url(r'^logout/', logout, {'next_page': 'website:home'}, name='logout'),
    url(r'^login/', views.LoginView.as_view(), name='login'),
    url(r'^perfil', views.update, name='perfil'),
    # url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', auth_views.password_reset_confirm, name='password_reset_confirm'),
    # url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
]
