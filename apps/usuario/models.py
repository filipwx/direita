from django.db import models
from django.contrib.auth.models import AbstractBaseUser, UserManager, PermissionsMixin

UF = (
    ('12', 'ACRE'),
    ('27', 'ALAGOAS'),
    ('16', 'AMAPÁ'),
    ('13', 'AMAZONAS'),
    ('29', 'BAHIA'),
    ('23', 'CEARÁ'),
    ('53', 'DISTRITO FEDERAL'),
    ('32', 'ESPÍRITO SANTO'),
    ('52', 'GOIÁS'),
    ('21', 'MARANHÃO'),
    ('51', 'MATO GROSSO'),
    ('50', 'MATO GROSSO DO SUL'),
    ('31', 'MINAS GERAIS'),
    ('15', 'PARÁ'),
    ('25', 'PARAÍBA'),
    ('21', 'PARANÁ'),
    ('26', 'PERNAMBUCO'),
    ('22', 'PIAUÍ'),
    ('33', 'RIO DE JANEIRO'),
    ('24', 'RIO GRANDE DO NORTE'),
    ('43', 'RIO GRANDE DO SUL'),
    ('11', 'RONDÔNIA'),
    ('14', 'RORAIMA'),
    ('42', 'SANTA CATARINA'),
    ('35', 'SÃO PAULO'),
    ('28', 'SERGIPE'),
    ('17', 'TOCANTINS'),
)
class Usuario(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('E-mail', max_length=50, unique=True)
    nome = models.CharField('Nome', max_length=255, null=False, blank=False, default='')
    username = models.CharField('Apelido', max_length=155, null=False, blank=False)
    estado_usuario = models.CharField('Estado', max_length=20, choices=UF, default='nao_informado')
    cidade_usuario = models.CharField('Cidade', max_length=90, null=False, blank=False, default='')
    is_staff = models.BooleanField('Equipe', default=False)
    is_active = models.BooleanField('Ativo', default=True)
    date_joined = models.DateTimeField('Data de entrada', auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'

    def __str__(self):
        return self.username or self.email

    def get_full_name(self):
        return str(self)

    def get_short_name(self):
        return str(self).split(" ")[0]
