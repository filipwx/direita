import json
import urllib
from django.views import generic
from django.contrib import messages
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import login, authenticate
from django.contrib.auth.views import password_reset, password_reset_done
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView, ListView, FormView, DetailView
from django.views.generic.edit import UpdateView
from django.conf.urls import url
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from .models import *
from .forms import CustomUserCreationForm, LoginForm, RecoverForm, CustomUserChangeForm
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.mail import send_mail, EmailMessage
import telegram


class SignUp(generic.CreateView):
  model = Usuario
  form_class = CustomUserCreationForm
  template_name = 'cadastro.html'

  def form_valid(self, form):
    recaptcha_response = self.request.POST.get('g-recaptcha-response')
    url = 'https://www.google.com/recaptcha/api/siteverify'
    values = {
        'secret': settings.RECAPTCHA_PRIVATE_KEY,
        'response': recaptcha_response
    }
    data = urllib.parse.urlencode(values).encode()
    req =  urllib.request.Request(url, data=data)
    response = urllib.request.urlopen(req)
    result = json.loads(response.read().decode())

    if result['success']:
        form.save()
        messages.success(self.request, 'Conta criada com sucesso')
        email = form.cleaned_data.get('email')
        raw_password = form.cleaned_data.get('password1')
        user = authenticate(email=email, password=raw_password)
        login(self.request, user)
        email_content = 'Novo cadastro de USUARIO '
        email_content += '\nNome: {}'.format(form.cleaned_data['nome'])
        email_content += '\Email: {}'.format(
            form.cleaned_data['email'])
        bot = telegram.Bot('752881370:AAHKmpMXALAUzt1d89UUvoCkayq5GPXqO0Y')
        chat_id = -1001440430219
        try:
            bot.send_message(chat_id=chat_id, text=email_content)
            pass
        except Exception as e:
            print(e)
            pass
        return redirect('website:home')
    else:
        messages.error(self.request, 'reCAPTCHA invalida. Tente novamente.')
        return render(self.request, 'cadastro.html', {'form': form})



class UserChange(UpdateView):
  model = Usuario
  form_class = CustomUserChangeForm
  template_name = 'edicao.html'

  def form_valid(self, form):
    form.save()
    messages.success(self.request, 'Cadastro editado com sucesso')
    return redirect('website:home')

@login_required
def update(request):

	if request.method == 'POST':
		form = CustomUserChangeForm(request.POST, instance=request.user)

		if form.is_valid():
			form.save()
			return redirect('website:home')

	else:
		form = CustomUserChangeForm(instance=request.user)

	context = RequestContext(request, {'form': form})
	return render_to_response('edicao.html', context)


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form, **kwargs):
        email = form.cleaned_data.get('email')
        raw_password = form.cleaned_data.get('password')
        token_check = Usuario.objects.filter(email=email)
        if token_check.exists():
            user = authenticate(email=email, password=raw_password)
            if user is None:
              errors = "Opsss, Email ou Senha invalidos!"
              messages.error(self.request, errors)
              return redirect('usuario:login')
            messages.success(self.request, 'Bem vindo de volta!')
            login(self.request, user)
            return redirect('website:home')
        else:
            return self.form_invalid(form)

    def form_invalid(self, form, **kwargs):
        print('Form invalido')
        context = super(LoginView, self).get_context_data(**kwargs)
        context['message_err'] = 'Verifique o email e a senha'
        return self.render_to_response(context)

def forgot_password(request):
    if request.method == 'POST':
        return password_reset(request, from_email=request.POST.get('email'))
    else:
        form = RecoverForm()
        return render(request, 'password_reset_form.html', {'form': form})


def password_reset_done(request):
    return password_reset_done(request, template_name='password_reset_done.html')
