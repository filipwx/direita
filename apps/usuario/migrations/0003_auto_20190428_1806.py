# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-04-28 21:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0002_auto_20190428_1515'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='nome',
            field=models.CharField(default='', max_length=255, verbose_name='Nome do Usuário'),
        ),
        migrations.AlterField(
            model_name='usuario',
            name='username',
            field=models.CharField(max_length=155, verbose_name='Apelido do Usuário'),
        ),
    ]
