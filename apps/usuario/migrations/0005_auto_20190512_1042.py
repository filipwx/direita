# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-05-12 13:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0004_auto_20190428_1900'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='estado_usuario',
            field=models.CharField(choices=[('12', 'ACRE'), ('27', 'ALAGOAS'), ('16', 'AMAPÁ'), ('13', 'AMAZONAS'), ('29', 'BAHIA'), ('23', 'CEARÁ'), ('53', 'DISTRITO FEDERAL'), ('32', 'ESPÍRITO SANTO'), ('52', 'GOIÁS'), ('21', 'MARANHÃO'), ('51', 'MATO GROSSO'), ('50', 'MATO GROSSO DO SUL'), ('31', 'MINAS GERAIS'), ('15', 'PARÁ'), ('25', 'PARAÍBA'), ('21', 'PARANÁ'), ('26', 'PERNAMBUCO'), ('22', 'PIAUÍ'), ('33', 'RIO DE JANEIRO'), ('24', 'RIO GRANDE DO NORTE'), ('43', 'RIO GRANDE DO SUL'), ('11', 'RONDÔNIA'), ('14', 'RORAIMA'), ('42', 'SANTA CATARINA'), ('35', 'SÃO PAULO'), ('28', 'SERGIPE'), ('17', 'TOCANTINS')], default='nao_informado', max_length=20, verbose_name='Estado'),
        ),
    ]
