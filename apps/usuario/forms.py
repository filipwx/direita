from django import forms
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import *
from django.forms import ModelForm


class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = Usuario
        captcha = ReCaptchaField(widget=ReCaptchaV2Checkbox(
            attrs={
                'data-theme': 'dark',
                'data-size': 'compact',
            }
        ))
        fields = ['nome', 'username', 'email', 'estado_usuario', 'cidade_usuario']


class CustomUserChangeForm(ModelForm):
    class Meta:
        model = Usuario
        fields = ['nome', 'username', 'email',
                'estado_usuario', 'cidade_usuario']
    cidade_usuario = forms.CharField(label="Cidade", widget=forms.TextInput(
        attrs={'id': 'cidade_usuario', 'type': 'text', 'class': 'validate'}))
    username = forms.CharField(label="Apelido", widget=forms.TextInput(
        attrs={'id': 'username', 'type': 'text', 'class': 'validate'}))
    nome = forms.CharField(label="Nome", widget=forms.TextInput(
        attrs={'id': 'nome', 'type': 'text', 'class': 'validate'}))
    email = forms.EmailField(label="Email", widget=forms.TextInput(
        attrs={'id': 'email', 'type': 'email', 'class': 'validate'}))
class LoginForm(forms.Form):
    email = forms.EmailField(label="Email", widget=forms.TextInput( attrs={'id': 'email', 'type': 'email', 'class': 'validate'}))
    password = forms.CharField(label="Senha", widget=forms.TextInput(attrs={'type': 'password'}))
class RecoverForm(forms.Form):
    email = forms.EmailField(label="Email", widget=forms.TextInput( attrs={'id': 'email', 'type': 'email', 'class': 'validate'}))
