from django.db import models

from versatileimagefield.fields import VersatileImageField



class Banner(models.Model):
    nome = models.CharField(max_length=155)
    titulo = models.CharField(max_length=255, default="0")
    imagem = models.CharField(max_length=555, unique=False)
    ativo = models.BooleanField(default=False)

class BannerAnuncio(models.Model):
    nome = models.CharField(max_length=155)
    url = models.URLField(max_length=555, default="")
    imagem = models.CharField(max_length=555, unique=False)
    ativo = models.BooleanField(default=False)

class BannerBitcoin(models.Model):
    nome = models.CharField(max_length=155)
    url = models.URLField(max_length=555, default="")
    imagem = models.CharField(max_length=555, unique=False)
    ativo = models.BooleanField(default=False)

class Titulo(models.Model):
    titulo = models.CharField(max_length=255, default="0")
    Subtitulo = models.CharField(max_length=255, default="0")
