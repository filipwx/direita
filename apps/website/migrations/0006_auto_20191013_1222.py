# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-10-13 15:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0005_bannerbitcoin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='imagem',
            field=models.CharField(max_length=555),
        ),
        migrations.AlterField(
            model_name='banneranuncio',
            name='imagem',
            field=models.CharField(max_length=555),
        ),
        migrations.AlterField(
            model_name='bannerbitcoin',
            name='imagem',
            field=models.CharField(max_length=555),
        ),
    ]
