$(document).ready(function() {
  $('select').material_select();
});

$("#alergia").change(function(){
  var select_active = $("#alergia li.active").text();
  if (select_active == "Sim") {
    $('textarea[name=alergia_texto]').attr("required", "true");
  }
  else {
    $('textarea[name=alergia_texto]').removeAttr("required");
  }
});

$("#objetivo").change(function(){
  var select_active = $("#objetivo li.active").text();
  if (select_active == "Outros") {
    $('textarea[name=descricao_objetivo]').attr("required", "true");
  }
  else {
    $('textarea[name=descricao_objetivo]').removeAttr("required");
  }
});
