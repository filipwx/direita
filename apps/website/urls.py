from django.conf.urls import url
from django.contrib.auth import views as auth_views
from apps.website.views import HomeView, QuemView, \
   ContatoView, ReceitasView, AreaView

app_name = 'website'

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
]
