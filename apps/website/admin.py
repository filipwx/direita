from django.contrib import admin
from apps.core.admin import DefaultAdmin

from .models import Banner, Titulo, BannerAnuncio, BannerBitcoin

# Register your models here.

@admin.register(Banner)
class BannerAdmin(DefaultAdmin):
    list_display = ('nome', 'imagem')


@admin.register(BannerAnuncio)
class BannerAnuncioAdmin(DefaultAdmin):
    list_display = ('nome', 'url')

@admin.register(BannerBitcoin)
class BannerBitcoinAdmin(DefaultAdmin):
    list_display = ('nome', 'url')


@admin.register(Titulo)
class TituloAdmin(DefaultAdmin):
    list_display = ('titulo',)
