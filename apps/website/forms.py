from django import forms
from apps.receitas.models import Receitas
# from apps.receitas.models import UM_TRES


class ContatoForm(forms.Form):
    nome = forms.CharField(label="Nome",
                           widget=forms.TextInput(
                               attrs={'id': 'name',
                                      'type': 'text',
                                      'class': 'validate'}))
    email = forms.EmailField(label="Email",
                             widget=forms.TextInput(
                                 attrs={'id': 'email',
                                        'type': 'email',
                                        'class': 'validate'}))
    telefone = forms.CharField(label="Telefone",
                               widget=forms.TextInput(
                                   attrs={'id': 'fone',
                                          'type': 'text',
                                          'class': 'validate'}))
    cidade = forms.CharField(label="Telefone",
                               widget=forms.TextInput(
                                   attrs={'id': 'city',
                                          'type': 'text',
                                          'class': 'validate'}))
    observacoes = forms.CharField(label="Mensagem",
                               widget=forms.Textarea(
                                   attrs={'id': 'textarea1',
                                          'class': 'materialize-textarea'}))

class ReceitaForm(forms.ModelForm):

    class Meta:
        model = Receitas
        exclude = ('',)
        widgets = {
            'observacoes': forms.Textarea(attrs={'id': 'textarea1',
                                          'class': 'materialize-textarea'}),
            'alergia_texto': forms.Textarea(attrs={'id': 'textarea1',
                                          'class': 'materialize-textarea materialize-textarea gray'}),
            'descricao_objetivo': forms.Textarea(attrs={'id': 'textarea1',
                                          'class': 'materialize-textarea materialize-textarea gray'})
        }

class LoginForm(forms.Form):
    token = forms.CharField(widget=forms.TextInput(attrs={'type': 'password'}))
