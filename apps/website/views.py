from django.contrib.sites.shortcuts import get_current_site
from django.views.generic import TemplateView, ListView, FormView, DetailView
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.core.mail import send_mail, EmailMessage
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from apps.website.models import Banner, Titulo, BannerAnuncio, BannerBitcoin
from apps.website.forms import ContatoForm, ReceitaForm, LoginForm
from apps.sobre.models import Valores, Quem, Missao, Visao, ImagemQuemSomos
from apps.receitas.models import Receitas
from apps.grupo.models import Grupos


# Create your views here.
class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        context.update(dict(
            banner_list=Banner.objects.filter(ativo=True)[:1],
            banner_anuncio_list=BannerAnuncio.objects.filter(ativo=True)[:1],
            banner_bitcoin_list=BannerBitcoin.objects.filter(ativo=True)[:1],
            grupos_list=Grupos.objects.all(),
            titulo=Titulo.objects.all()[:1]
        ))
        return context


# class LoginView(FormView):
#     template_name = 'login.html'
#     form_class = LoginForm
#     success_url = '/area'

#     def form_valid(self, form, **kwargs):
#         print('Form valido')
#         token_try = form.cleaned_data['token']
#         token_check = pro_models.Token.objects.filter(token=token_try)
#         if token_check.exists():
#             self.request.session["token_active"] = True
#         else:
#             return self.form_invalid(form)
#         return HttpResponseRedirect(self.get_success_url())

#     def form_invalid(self, form, **kwargs):
#         print('Form invalido')
#         context = super(LoginView, self).get_context_data(**kwargs)
#         # send_mail()
#         context['message_err'] = 'O token digitado é invalido'
#         return self.render_to_response(context)


class QuemView(TemplateView):
    template_name = 'quem.html'

    def get_context_data(self, **kwargs):
        context = super(QuemView, self).get_context_data(**kwargs)

        context.update(dict(
            missao=Missao.objects.filter(ativo=True).last(),
            quem=Quem.objects.filter(ativo=True).last(),
            visao=Visao.objects.filter(ativo=True).last(),
            valores=Valores.objects.filter(ativo=True)[:6],
            imagem_quem=ImagemQuemSomos.objects.filter(ativo=True).last()
        ))
        return context


class AreaView(ListView):
    template_name = 'area.html'

    def get_context_data(self, **kwargs):
        context = super(AreaView, self).get_context_data(**kwargs)
        context['categoria_list'] = pro_models.Categoria.objects.all()
        return context

    def find_categoria(self, cat_name):
        return pro_models.Categoria.objects.filter(nome_categoria=cat_name)

    def dispatch(self, request, *args, **kwargs):
        token_check = request.session.get('token_active', False)
        if not token_check:
            return HttpResponseRedirect('/login/')
        return super(AreaView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        search = self.request.GET.get('search', None)
        cat = self.request.GET.get('cat', None)
        if search:
            return pro_models.Pdf.objects.filter(
                (Q(titulo__icontains=search) |
                 Q(categoria_fk__nome_categoria__icontains=search)))
        elif cat:
            return pro_models.Pdf.objects.filter(
                categoria_fk=self.find_categoria(cat))
        return pro_models.Pdf.objects.all()


class ContatoView(FormView):
    template_name = 'contato.html'
    form_class = ContatoForm
    sucess_url = '/contato'

    def form_valid(self, form, **kwargs):
        # print('Form valido')
        context = super(ContatoView, self).get_context_data(**kwargs)
        email_content = ''
        for field, val in form.cleaned_data.items():
            if val:
                email_content += '\n{}: {}'.format(field.title(), val)

        send_mail(
            'Contato - %s' % (form.cleaned_data['nome']), email_content,
            'tester@gmail.com',
            ['tester@gmail.com'], fail_silently=False,
        )
        # print('nome', form.cleaned_data['nome'])
        # send_mail()
        context['message'] = 'Mensagem enviada com sucesso!'
        return self.render_to_response(context)

    def form_invalid(self, form, **kwargs):
        # print('Form invalido')
        context = super(ContatoView, self).get_context_data(**kwargs)
        # print('nome', form.cleaned_data['nome'])
        context['message_err'] = 'Preencha todos os campos corretamente'
        return self.render_to_response(context)


class ReceitasView(FormView):
    template_name = 'receitas.html'
    form_class = ReceitaForm
    sucess_url = '/receitas'

    def form_valid(self, form, **kwargs):
        context = super(ReceitasView, self).get_context_data(**kwargs)
        data = form.save()
        url = Receitas.objects.last()
        email_list = [
            'tester@gmail.com'
        ]
        email = email_list[0]

        email_content, i = '', 0
        for field, val in form.cleaned_data.items():
            if val and field != 'receita':
                format_field = data._meta.get_field(field).verbose_name
                email_content += '\n{}: {}'.format(format_field.title(), val)
            elif field == 'receita':
                url = get_current_site(self.request).domain
                email_content += '\n{0}: http://{1}{2}'.format(
                    field.title(), url, data.receita.url,
                )
            if i >= 7 and val:
                email = email_list[0]
            i += 1
        try:
            email_content += '\nReceita1 Link: {}'.format('http://principioativo.far.br' + url.receita1.url)
            email_content += '\nReceita2 Link: {}'.format('http://principioativo.far.br' + url.receita2.url)
            email_content += '\nReceita3 Link: {}'.format('http://principioativo.far.br' + url.receita3.url)
        except:
            print('não tem todas as receitas')

        send_mail(
            'Nova Receita - %s' % (form.cleaned_data['nome']), email_content,
            'tester@gmail.com',
            [email], fail_silently=False,
        )
        # orcamento@principiotivo.far.br
        context['message'] = 'Mensagem enviada com sucesso!'
        return self.render_to_response(context)

    def form_invalid(self, form, **kwargs):
        context = super(ReceitasView, self).get_context_data(**kwargs)
        context['message_err'] = 'Preencha todos os campos corretamente'
        return self.render_to_response(context)
