from django.contrib import admin

from reversion.admin import VersionAdmin

# Register your models here.

class DefaultAdmin(VersionAdmin):
    save_as = True

