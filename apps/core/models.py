from django.db import models

# Create your models here.

# A abstract class to be replace the main class
class DefaultModel(models.Model):
    criador = models.CharField(max_length=100, null=True, blank=True)
    criado_em = models.DateTimeField(auto_now=True)
    modificado_em = models.DateTimeField(auto_now_add=True)

    def save(self):
        try:
            criador = self.request.user()
        except:
            pass
        criador.save()

