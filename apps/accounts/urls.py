from django.conf.urls import url
from django.contrib.auth.views import logout
from django.conf import settings
from . import views


urlpatterns = [
    url(r'^cadastro/$', views.SignUp.as_view(), name='signup'),
    url(r'^logout/$', logout, {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout')
]
