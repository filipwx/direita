from django.db import models

from versatileimagefield.fields import VersatileImageField

# Create your models here.
class Quem(models.Model):
    texto = models.TextField('Texto',max_length=600)
    ativo = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Quem'
        verbose_name_plural = 'Quem'

    def __str__(self):
        return '{}...'.format(self.texto[:15])


class Missao(models.Model):
    texto = models.TextField(max_length=600)
    ativo = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Missao'
        verbose_name_plural = 'Missao'

    def __str__(self):
        return '{}...'.format(self.texto[:15])


class Valores(models.Model):
    texto = models.TextField(
            max_length=21,
            help_text="Máximo de 21 caracteres"
        )
    ativo = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Valores'
        verbose_name_plural = 'Valores'

    def __str__(self):
        return '{}'.format(self.texto)


class Visao(models.Model):
    texto = models.TextField(max_length=600)
    ativo = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Visao'
        verbose_name_plural = 'Visao'

    def __str__(self):
        return '{}...'.format(self.texto[:15])


class ImagemQuemSomos(models.Model):
    imagem = VersatileImageField(upload_to='quem-somos/%Y/%m/')
    ativo = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Imagem - Quem Somos'
        verbose_name_plural = 'Imagem - Quem Somos'

    def __str__(self):
        return '{}'.format(self.imagem)

    def save(self):
        if self.ativo:
            try:
                to_deactivate = ImagemQuemSomos.objects.get(ativo=True)
                if self != to_deactivate:
                    to_deactivate.ativo = False
                    to_deactivate.save()

            except ImagemQuemSomos.DoesNotExist:
                pass
        super(ImagemQuemSomos, self).save()
