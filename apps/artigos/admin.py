from django.contrib import admin
from apps.core.admin import DefaultAdmin
from .models import Autor, Artigos


@admin.register(Autor)
class AutorAdmin(DefaultAdmin):
  list_display = ('nome', 'criado_em')

@admin.register(Artigos)
class ArtigosAdmin(DefaultAdmin):
  list_display = ('autor', 'titulo', 'categoria', 'slug')
  class Media:
    js = (
        '/static/noticia/js/tinymce/js/tinymce/tinymce.min.js',
        '/static/noticia/js/tinymce/js/tinymce/tinymce_setup.js',
    )
