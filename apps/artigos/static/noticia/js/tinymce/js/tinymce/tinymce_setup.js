var tinySettings = {
  selector: 'textarea',
  language_url : '/static/noticia/js/tinymce/js/tinymce/langs/pt_BR.js',
  height: 500,
  relative_urls: false,
  remove_script_host : true,
  paste_as_text: true,
  document_base_url : "/",
  image_advtab: true,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
  content_css: [
    // '/static/frontend/css/app.css',
    //'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',   //'//www.tinymce.com/css/codepen.min.css'
  ],
  removeformat : [
      {selector : 'b,strong,em,i,font,u,strike', remove : 'all', split : true, expand : false, block_expand : true, deep : true},
      {selector : 'span', attributes : ['style', 'class'], remove : 'empty', split : true, expand : false, deep : true},
      {selector : '*', attributes : ['style', 'class'], split : false, expand : false, deep : true}
    ],
  formats : {
    // titulo: {selector : 'h1,h2,h3,h4,h5,h6', classes : 'color-orange'},
    // paragrafo: {selector: 'p,ul,li,ol,table', classes: 'color-gray'},
    // custom_format: {block : 'h1', attributes : {title : 'Header'}, styles : {color : 'red'}}
  },
  style_formats: [
    //{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
    // {title: 'Título', block: 'h2', classes: 'color-orange'},
    // {title: 'Parágrafo', block: 'p', classes: 'color-gray'},
  ],
  file_browser_callback : function(field_name, url, type, win) {
    var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

    //var cmsURL = 'index.html?&field_name='+field_name+'&langCode='+tinymce.settings.language;
    var cmsURL = '/manage-cms/filemanager/?path='
    if(type == 'image') {
        cmsURL = cmsURL + "&type=images";
    }

    tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
    },{
        window: win,
        input: field_name
    });
    return false;
  }
}

tinymce.init(tinySettings);
