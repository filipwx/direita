from django.views import generic
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView, FormView, DetailView
from django.views.generic.edit import UpdateView
from django.conf.urls import url
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from .models import *
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail, EmailMessage
from django.conf import settings

# Create your views here.


class ArtigosView(ListView):
    template_name = 'noticias.html'
    model = Artigos
    paginate_by = 5
    context_object_name = 'artigos_list'
    queryset = Artigos.objects.all().order_by('-criado_em')

    def get_queryset(self):
        return Artigos.objects.all().order_by('-criado_em')

    def get_context_data(self, **kwargs):
        context = super(ArtigosView, self).get_context_data(**kwargs)
        if str(self.request.user) == "AnonymousUser":
            user_loged = False
            pass
        else:
            user_loged = True
            pass

        context.update(dict(
            artigos=Artigos.objects.all().order_by('-criado_em'),
            user_loged=user_loged,
        ))
        return context


class ArtigosInternaView(DetailView):
    template_name = 'noticia-interna.html'
    model = Artigos

    def get_context_data(self, **kwargs):
        context = super(ArtigosInternaView, self).get_context_data(**kwargs)

        context.update(dict(
            noticias_list=Artigos.objects.all().order_by("criado_em")[:3],
        ))

        return context


class ArtigoView(ListView):
    template_name = 'saude.html'
    paginate_by = 10

    def get_queryset(self):
        try:
            get_categoria = self.request.GET['cat']
            return Artigos.objects.filter(categoria=get_categoria)

        except KeyError:
            return Artigos.objects.all()

        return Artigos.objects.all()

    def get_context_data(self, **kwargs):
        context = super(ArtigoView, self).get_context_data(**kwargs)

        return context
