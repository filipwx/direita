from django.conf.urls import url
from django.contrib.auth.views import logout, login
from django.conf import settings

from . import views

app_name = 'artigo'

urlpatterns = [
    url(r'^lista-de-artigos/$', views.ArtigosView.as_view(), name='artigos'),
    url(r'^artigo/(?P<slug>[-\w]+)/$',views.ArtigosInternaView.as_view(), name='artigo-interno')
]
