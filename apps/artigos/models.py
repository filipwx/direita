from django.db import models
from django.utils.text import slugify

from tinymce.models import HTMLField
from versatileimagefield.fields import VersatileImageField

from random import randrange
# from apps.noticias.admin import slug_validator

class Artigos(models.Model):
    autor = models.ForeignKey('artigos.Autor', on_delete=models.PROTECT, related_name="artigo_autor")
    titulo = models.CharField(max_length=155, unique=True)
    categoria = models.CharField(max_length=155, unique=False)
    noticia = models.TextField('Notícia')

    imagem = models.CharField(max_length=555, unique=False)

    slug = models.SlugField(max_length=700)

    criado_em = models.DateTimeField(
        verbose_name='Criado em', auto_now_add=True)
    modificado_em = models.DateTimeField(
        verbose_name='Modificado em', auto_now=True)

    class Meta:
        verbose_name = 'Artigos'
        verbose_name_plural = 'Artigos'

    def __str__(self):
        return '{}'.format(self.titulo)

    def save(self):
        self.slug = slugify(self.titulo)
        while True:
            noticia = Artigos.objects.filter(slug=self.slug)
            if noticia.exists():
                self.slug = '{}{}'.format(self.slug, randrange(10000000000))
            else:
                break

        super(Artigos, self).save()


class Autor(models.Model):
    nome = models.CharField(
        max_length=25, help_text='Maximo de 25 caracteres')
    criado_em = models.DateTimeField(
        verbose_name='Criado em', auto_now_add=True)
    modificado_em = models.DateTimeField(
        verbose_name='Modificado em', auto_now=True)

    class Meta:
        verbose_name_plural = 'Autores'

    def __str__(self):
        return '%s' % (self.nome)
